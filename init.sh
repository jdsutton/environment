#!/bin/bash

sudo add-apt-repository ppa:webupd8team/sublime-text-3
sudo add-apt-repository ppa:jonathonf/python-3.6

sudo apt update

sudo apt install vim sublime-text python3.6 python3-pip virtualenv
